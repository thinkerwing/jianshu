import axios from 'axios';
import * as constants from './constants';
import '../../api'
const changeLogin = () => ({
	type: constants.CHANGE_LOGIN,
	value: true
})

export const logout = () => ({
	type: constants.LOGOUT,
	value: false
})

export const login = (accout, password) => {
	return (dispatch) => {
		// axios.get('/api/login.json?account=' + accout + '&password=' + password).then((res) => {
			axios.get('/mock/login').then((res) => {
			const result = res.data.data;
			if (result) {
				dispatch(changeLogin())
			}else {
				alert('登陆失败')
			}
		})
	} 
}