import Mock from 'mockjs'
let logins = require('./login.json')
let notice = function (option) {
    let info = JSON.parse(option.body)
    return {
      data: logins
    }
  }
Mock.mock('/mock/login', 'get', notice)

let homes = require('./home.json')
let home = function (option) {
    let info = JSON.parse(option.body)
    return {
      data: homes
    }
  }
Mock.mock('/mock/home', 'get', home)

let details = require('./detail.json')
let detail = function (option) {
    let info = JSON.parse(option.body)
    console.log(option);
    return {
      data: details
    }
  }
Mock.mock('/mock/detail', 'get', detail)

let homeLists = require('./homeList.json')
let homeList = function (option) {
    let info = JSON.parse(option.body)
    console.log(option);
    return {
      data: homeLists
    }
  }
Mock.mock('/mock/homeList', 'get', homeList)