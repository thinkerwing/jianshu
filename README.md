项目介绍： https://blog.csdn.net/daddykei/article/details/119191099
运行：yarn start
public/api 存放本地模拟数据

react项目打包后运行，在生产环境下
npm/yarn build后
无法运行或者是资源路径出错
一. 在开发模式下，package.json下设置 homepage
“homepage”:".",
以上设置会将 index.html中的%PUBLIC_URL%替换为 .,从而拼接为：./index.js这样的一个相对index.html的资源路径.

二. react使用的browserHistory模式项目，部署到服务器上的子目录中，访问时页面空白，没有报错
原因：我们使用react-router-dom里的BrowserRouter as Router时，是用浏览器history对象的方法去请求服务器，如果服务器没有相对于的路由去指向对应的页面路由会找不到资源。
解决： 两种方案：要么改用HashRouter,要么让运维对nginx.conf进行新的配置（其他代理服务器同理）
BrowserRouter：使用于现代浏览器，支持H5 history API，但使用它还需要和服务器部署联系，修改对应的服务器配置
HashRouter：常用于旧款浏览器（兼容性更好），格式如come/index.html#/home
3.打包后 “/”这个路径找不到资源的情况下，可以在去掉，，例如 “static” 替换成"/static"，直接在当前index.html页面在本地服务器运行
